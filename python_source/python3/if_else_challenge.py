#!/bin/python3

import math
import os
import random
import re
import sys

# Condition 1 If n > 10 print(something_1)
# Condition 2 If n > 10 and n < 20 print (something_2)
# Condition 3 If n < 10 and n > 0 print (something_3)
# Condition 4 If n > 10 and n > 30 print(something_4)

# Why do we need to strip input the input? 
# Not really sure lolz, but I found this https://www.geeksforgeeks.org/python-string-strip-2/
# I guess it's mainly for removing unwanted characters from inputs lolz
# Hmm I just realized I may want to add an auto-testing or rather algorithm validation tool

# Ah hmm something better to define the above cases:
# Straightforward version for the constraints is:
def straightForward(n):
    if (n > 10):
        print ("something_1")
    if ((n > 10) and (n < 20 )):
        print ("something_2")
    if ((n < 10) and (n > 0)):
        print ("something_3")
    if ((n > 10) and (n > 30)):
        print ("something_4)

# Now here's the over analytics use case:
# Condition 1 can be valid if n > 10 and n >=20 and n <=30
# Condition 2 can be valid if n > 10 and n < 20
# Condition 3 can be valid if n > 0 and n < 10
# Condition 4 can be valid if n > 10 and n > 30 | can also be n > 30 since 30 > 10
# However what if n = 10? add a catch all 
def overAnalyze(n):
    if (n>30):
        print("something_4")
    elif ((n > 0) and (n < 10)):
        print("something_3")
    else (n > 10):
        if (n < 20): 
            print("something_2")
        elif((n >= 20) and (n <=30)):
            print("something_1")
        else:
            print("invalid")
            
if __name__ == '__main__':
    n = int(input().strip())
    straightForward(n)
    overAnalyze(n)
